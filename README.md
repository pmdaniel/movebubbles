pmconfig contents (without comments!):

fadeinlim	100 // (int) distance from the bottom of the window, where new news items will start fading in
fadeoutlim	500 // (int) distance from the top of the window, where items will start fading out
rndlowerlim	0.8 // (float) lower limit of random generator for relative speed
rndupperlim	1.1 // (float) upper limit of random generator for relative speed
movelength	1 // (float) this number corresponds to the length of the step that the bubbles make in each turn
speed	1 // (float) this number tells how fast the movement should be
nmax	5 // (int) max number of bubbles. i 0, then the limit is turned off

-----------------------------------------------------------------------
pmswitches.h contents:

#pragma once

#define PMDEBUG 0
#define DRAWRECTS 1 && PMDEBUG
#define WAVE 0

-----------------------------------------------------------------------
Controls:
- ' ': adds a new bubble and pushes the whole thing one step up, along a harmonic wave.
- 'N': triggers "No more please', so on further pushes no more bubbles will be added, only the existing bubbles will fly upwards.