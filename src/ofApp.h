#pragma once

#include "pmswitches.h"
#include "ofMain.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "parameters.h"

struct BUBBLE {
	ofImage img;
	ofPoint position;
#if WAVE
	float time;
#else
	ofPoint moveDir;
	float relSpeed;
	bool letFadeIn;
#endif
	bool letFadeOut;
	// we assume that the sizes can be different
	float width;
	float height;
	float alpha;

	BUBBLE(const float &t, const ofImage &srcImage, const float &x, const float &y, const float &w, const float &h);
	void setPos(const ofPoint &pos);
	void setPos(const float &x, const float &y);
	void adjustAlpha(const float &a);

#if !WAVE
	void move(const float &speed);
#endif

};

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

#if PMDEBUG
		ofTrueTypeFont font;
#endif
		bool stop;

		float dt = 0.001;
#if WAVE	
		float speed;
		int last = 0;
		ofPoint getCosine(const float &ampl, const float &waveLength, const float &time, bool &isExt, const float &xoffset, const float &yoffset, const float &step = 10, bool up=true);


		float offSet;
		float lastPeriod = 0.5;
		float yperiod = 200;

		int n = 0;
#else
		float globalSpeed;
		int countBub = 0;

		PARAMETERS params;

		float time;
#endif; // WAVE

		
		bool keepAdding;

		deque<BUBBLE> bubbles;

		int nBubble = 4;
		vector<ofImage> images;
		int current;

		ofImage imgBackGround;
		ofImage imgClock;

		int clockHeight;

#if PMDEBUG
		int textYpos = 0;
		int textStep(const int &step = 20);
#endif
};
