#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
#if PMDEBUG
	font.load("mono.ttf", 9);
#endif // PMDEBUG
#if WAVE
	offSet = ofGetWidth() / 2;
	speed = 0.01;
#endif // WAVE
	stop = true;
	keepAdding = true;
	bubbles.clear();
	images.clear();

	string configFile = "pmconfig";
	if (!params.readConfig(configFile)) {
		params.setDefaultConfig();
		cout << "Failed to load config data from " + configFile + ". Using default configuration." << endl;
	}

	for (int i = 0; i < nBubble; i++) {
		string loc = "images/MGM_new" + ofToString(i + 1) + ".png";
		ofImage img(loc);
		img.setImageType(OF_IMAGE_COLOR_ALPHA);
		images.push_back(img);
	}
	
	imgBackGround.load("images/MGM_morning_1.png");
	imgBackGround.setImageType(OF_IMAGE_COLOR_ALPHA);
	imgClock.load("images/MGM_morning_clock.png");
	imgClock.setImageType(OF_IMAGE_COLOR_ALPHA);

	current = 0;
	clockHeight = 357;
	ofEnableAlphaBlending();
}

#if WAVE
// in experimental state
float speedFunc(const float &max, const float &t, const float &length) {
	//return max*( -0.5*cos(x*M_PI / length)+0.5)+0.5;
//	return max*0.5*(-cos(max*M_PI*t / length) + 1);
	float ratio = 0.1;
	if (t <= 0)
		return 1;
	if (t > 0 && t < length*ratio)
		return t*max / ratio+1;
	if (t >= length*ratio && t < length*(1 - ratio))
		return max+1;
	if (t >= length*(1 - ratio) && t<length)
		return -t*max / ratio+max*length/ratio+1;
	if (t >= length)
		return speedFunc(max, t-length, length);
}
#endif // WAVE

float speedFunc(const float &time, const float &T, const float &alfa = 0.5f) {
	if (T == 0) {
		cout << "ERROR: div by zero." << endl;
		return 0;
	}
	float arg = M_PI*time / T;
	return alfa*(1 - cos(arg)*cos(arg));
}

#if WAVE
float fadeFunc(const float &x, const float &start = 400, const float &end = 0) {
	if (x > start)
		return 1;
	if (x <= start && x > end)
		return 0.5*(1 - cos(x / (start / M_PI)));
	//if (x <= end)
	else
		return 0;
}
#endif

float fadeInOut(const float &t, const float &T, bool in) {
	return in ? 0.5*(1 - cos(t*M_PI / T)) : 0.5*(1 + cos(t*M_PI / T));
}

//--------------------------------------------------------------
void ofApp::update(){
	if (!stop) {
#if !WAVE
		float T = params.length;
		dt = 1 / ofGetFrameRate();
		globalSpeed = speedFunc(time, T, params.speed) / ofGetFrameRate();
#endif // !WAVE
		for (int i = 0; i < bubbles.size(); i++) {
#if PMDEBUG
			ofPoint prevPos = bubbles[i].position;
#endif // PMDEBUG
#if WAVE
			bool locext = false;
			bubbles[i].position = getCosine(200, yperiod, bubbles[i].time, locext, offSet - bubbles[i].width / 2, ofGetHeight(), 350);
			bubbles[i].time += dt;
#else
			bubbles[i].move(globalSpeed);

			
#endif // WAVE

#if PMDEBUG
			//if (locext) {
			if(prevPos.y<bubbles[i].position.y){
				cout << "";
			}
			else {
				cout << "";
			}
#endif // PMDEBUG
#if WAVE
			if (i == 0) {
				stop = locext;
			}
#endif // WAVE

		}
#if !WAVE
		time += dt;
		if (time >= T) {
			stop = true;
		}
#endif // !WAVE

		if (bubbles.size() > 0) {
			if (bubbles[0].letFadeOut) {
#if WAVE
				bubbles[0].adjustAlpha(255 * fadeFunc(bubbles[0].position.y, 500, 150));
#else
				bubbles[0].adjustAlpha(255 * fadeInOut(time, T, false));
#endif // WAVE
			}
//#if WAVE
//			speed = speedFunc(350, bubbles[0].time, yperiod/2);
//#endif
			if(bubbles[0].alpha<5 && bubbles[0].letFadeOut){
				bubbles.pop_front();
#if WAVE
				last--;
#endif // WAVE

			}
			if (bubbles.size() == 0) {
				ofExit(); 
				return;
			}
			if (bubbles[bubbles.size() - 1].letFadeIn) {
				float fade = fadeInOut(time, T, true);
				if (fade > 0.95) {
					bubbles[bubbles.size() - 1].adjustAlpha(255);
					bubbles[bubbles.size() - 1].letFadeIn = false;
				}
				else {
					bubbles[bubbles.size() - 1].adjustAlpha(255 * fade);
				}
			}
		}
	}
}

#if PMDEBUG
int ofApp::textStep(const int &step) {
	textYpos += step;
	return textYpos;
}
#endif // PMDEBUG

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetColor(255, 255, 255, 255);
	imgClock.draw(0, 0);
	imgBackGround.draw(0, 0);

	for (int i = 0; i < bubbles.size(); i++) {
		ofSetColor(255, 255, 255, bubbles[i].alpha);
		bubbles[i].img.draw(bubbles[i].position);
	}

#if PMDEBUG
	ofPushStyle();
	ofSetHexColor(0xffff00);
	textYpos = 0;
	string text = "FPS: " + ofToString(ofGetFrameRate());
	font.drawString(text, 20, textStep());
	font.drawString(ofToString(time), 20, textStep());
	font.drawString(ofToString(bubbles.size()), 20, textStep());
	ofDrawLine(ofPoint(ofGetWidth() / 2, 0), ofPoint(ofGetWidth() / 2, ofGetHeight()));
#if DRAWRECTS
	ofSetHexColor(0xff0000);
	ofNoFill();
	for (int i = 0; i < bubbles.size(); i++) {
		ofDrawRectangle(bubbles[i].position, bubbles[i].img.getWidth(), bubbles[i].img.getHeight());
		font.drawString(ofToString(bubbles[i].alpha), bubbles[i].position.x, bubbles[i].position.y + 20);
	}
#endif // DRAWRECTS

	ofPopStyle();
#endif // PMDEBUG
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch (key) {
	case ' ':
		if (stop) {
			if (keepAdding){
#if WAVE
				BUBBLE bubble(0, images[current], 0, 0, images[current].getWidth(), images[current].getHeight());
#else

				float x = countBub % 2 ? ofGetWidth() / 4 - images[current].getWidth() / 2 : ofGetWidth() * 3 / 4 - images[current].getWidth() / 2;
				float y = ofGetHeight()-params.fadeInLim;

				BUBBLE bubble(0, images[current], x, y, images[current].getWidth(), images[current].getHeight());
				
				countBub++;
#endif //WAVE
				bubbles.push_back(bubble);
#if !WAVE
				for (int i = 0; i < bubbles.size(); i++) {
					bubbles[i].relSpeed = ofRandom(params.RNDLowerLim, params.RNDUpperLim);
				}
#endif // !WAVE
				
				current++;
				if (current == nBubble) {
					current = 0;
				}
			}
			else {
				params.nmax = bubbles.size() - 1;
			}

			if (params.nmax) {
				if (bubbles.size() > params.nmax) {
					bubbles[0].letFadeOut = true;
				}
			}
			else if (bubbles.size() > 0) {
				if (bubbles[0].position.y < params.fadeOutLim) {
					bubbles[0].letFadeOut = true;
				}
			}
			stop = false;
#if !WAVE
			time = 0;
#endif // !WAVE
		}
		break;
	case 'n': // No more
		keepAdding = false;
		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

#if WAVE
//--------------------------------------------------------------
ofPoint ofApp::getCosine(const float &ampl, const float &waveLength, const float &time, bool &isExt, const float &xoffset, const float &yoffset, const float &step, bool up) {
	float y = step*time;
	float magicNumber = 0.8627;
	float k = 2 * M_PI / (waveLength*magicNumber);
	float omega = step / waveLength;
	float sine = cos(k*y - time*omega);
	float x = ampl*sine/2;

#if PMDEBUG
	if (y < 0)
		cout << "";
#endif // PMDEBUG

	if (((int)(y / (waveLength*lastPeriod)) > last)) {
		isExt = true;
		last = (int)(y / (waveLength*lastPeriod));
	}
	else {
		isExt = false;
	}

	return ofPoint(x + xoffset, up ? yoffset - y : yoffset + y);
}
#endif // WAVE

BUBBLE::BUBBLE(const float &t, const ofImage &srcImage, const float &x, const float &y, const float &w, const float &h) {
	img.setFromPixels(srcImage.getPixels());
	position.x = x;
	position.y = y;
	width = w;
	height = h;
	alpha = 0;// 255;
	letFadeOut = false;
#if WAVE
	time = t;
#else
	moveDir = ofPoint(0, -1);
	relSpeed = 0;
	letFadeIn = true;
#endif
}

void BUBBLE::setPos(const ofPoint &pos) {
	position = pos;
}

void BUBBLE::setPos(const float &x, const float &y) {
	position = ofPoint(x, y);
}

void BUBBLE::adjustAlpha(const float &a) {
	if (letFadeOut) {
		if (a < alpha) {
			alpha = a;
			return;
		}
	}
	if (letFadeIn) {
		if (a > alpha) {
			alpha = a;
			return;
		}
	}
}

#if !WAVE
void BUBBLE::move(const float &sp) {
	position += moveDir*sp*relSpeed;
}
#endif