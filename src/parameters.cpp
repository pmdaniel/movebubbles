#include "parameters.h"

PARAMETERS::PARAMETERS(){
	setDefaultConfig();
}

void PARAMETERS::setDefaultConfig() {
	fadeInLim = 100; // distance from the bottom of the window, where new news items will start fading in
	fadeOutLim = 500; // distance from the top of the window, where items will start fading out
	RNDLowerLim = 0.8; // lower limit of random generator for relative speed
	RNDUpperLim = 1.1; // upper limit of random generator for relative speed
	length = 1; // this number corresponds to the length of the step that the bubbles make in each turn
	speed = 1; // this number tells how fast the movement should be
	nmax = 5; // max number of bubbles. i 0, then the limit is turned off
}

bool PARAMETERS::readConfig(const string &fileName) {
	ifstream file;
	file.open(fileName, ios::in);
	if (!file.is_open())
		return false;
	string item = "";
	
	file >> item;
	if (item == "fadeinlim")
		file >> fadeInLim;
	else return false;

	file >> item;
	if (item == "fadeoutlim")
		file >> fadeOutLim;
	else return false;

	file >> item;
	if (item == "rndlowerlim")
		file >> RNDLowerLim;
	else return false;

	file >> item;
	if (item == "rndupperlim")
		file >> RNDUpperLim;
	else return false;

	file >> item;
	if (item == "movelength")
		file >> length;
	else return false;

	file >> item;
	if (item == "speed")
		file >> speed;
	else return false;

	file >> item;
	if (item == "nmax")
		file >> nmax;
	else return false;
	
	return true;
}
