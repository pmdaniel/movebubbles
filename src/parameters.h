#pragma once

#include <fstream>
#include <string>

using namespace std;

class PARAMETERS
{
public:
	int fadeInLim; // distance from the bottom of the window, where new news items will start fading in
	int fadeOutLim; // distance from the top of the window, where items will start fading out
	float RNDLowerLim; // lower limit of random generator for relative speed
	float RNDUpperLim; // upper limit of random generator for relative speed
	float length;
	float speed;
	int nmax;

	PARAMETERS();
	bool readConfig(const string & fileName);
	void setDefaultConfig();
};

